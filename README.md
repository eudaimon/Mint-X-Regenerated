# Mint-X-Regenerated

Mint-X is for me one of the best skeuomorphic themes ever made: extremely legible both in its light and dark variation. This is a small variation of it, mainly in the form of less bright colors, so it is easier on the eyes.
I've fixed an issue with some treeviews (Inkscape at least) not showing selected elements (because background-color was set to transparent).

More modifications will be coming (at least I'll add shadow undershoots from some other themes of mine, at least).

Kvantum theme is a modification of Greybird theme.

# Instructions

To edit colors for Mint-X, edit the following files:
- src/Mint-X/theme/Mint-X/gtk-2.0/gtkrc
- src/Mint-X/theme/Mint-X/gtk-3.0/sass/_colors.scss
- src/Mint-X/theme/Mint-X/gtk-4.0/sass/_colors.scss

To generate themes:

`./generate-themes.py`

This completely erases and regenerates the folder `generated-themes`. TODO: only update changed files, not replace all of them.

Once generated, you can add symbolic links to the themes you want to have. For example, supposing you have cloned the repo into `~/Themes/Mint-X-Regenerated`:

```sh
cd ~/.themes
ln -s ../Themes/Mint-X-Regenerated/generated-themes/Mint-X
ln -s ../Themes/Mint-X-Regenerated/generated-themes/Mint-Y
ln -s ../Themes/Mint-X-Regenerated/generated-themes/Mint-Y-dark
```

This would have linked only the "standard" Mint-X and Mint-Y themes, without the color variations.

Now each time you want to update the theme, you don't need to recreate the symbolic links, just execute `./generate-themes.py` after editing the files.

